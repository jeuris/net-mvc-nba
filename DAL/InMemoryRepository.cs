﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Basketbal.BL.Domain;

namespace Basketbal.DAL
{
    public class InMemoryRepository : IRepository
    {
        public static List<Match> _matches = new List<Match>();
        public static List<Referee> _referees = new List<Referee>();
        public static List<Arena> _arenas = new List<Arena>();

        static InMemoryRepository()
        {
            Seed();
        }

        static void Seed()
        {
            Arena arena1 = new Arena("FedExForum", "Memphis", 18199);
            arena1.ArenaId = _arenas.Count + 1;
            _arenas.Add(arena1);
            Arena arena2 = new Arena("Pepsi Center", "Denver", 20000);
            arena2.ArenaId = _arenas.Count + 1;
            _arenas.Add(arena2);
            Arena arena3 = new Arena("TD Garden", "Boston", 18624);
            arena3.ArenaId = _arenas.Count + 1;
            _arenas.Add(arena3);

            Match match1 = new Match("Boston Celtics", "Philadelphia 76ers",
                new DateTime(2020, 2, 2), arena3);
            match1.MatchId = _matches.Count + 1;
            _matches.Add(match1);
            Match match2 = new Match("Denver Nuggets", "Houston Rockets",
                new DateTime(2020, 1, 26), arena2);
            match2.MatchId = _matches.Count + 1;
            _matches.Add(match2);
            Match match3 = new Match("Boston Celtics", "Miami Heat",
                new DateTime(2020, 9, 23), arena3);
            match3.MatchId = _matches.Count + 1;
            _matches.Add(match3);
            Match match4 = new Match("Denver Nuggets", "Dallas Mavericks",
                new DateTime(2019, 10, 29), arena2);
            match4.MatchId = _matches.Count + 1;
            _matches.Add(match4);
            Match match5 = new Match("Memphis Grizzlies", "Washington Wizards",
                new DateTime(2019, 12, 30), arena1);
            match5.MatchId = _matches.Count + 1;
            _matches.Add(match5);

            Referee ref1 = new Referee("Josh Tiven", 58, 0.6, RefereeType.ALTERNATE);
            ref1.RefereeId = _referees.Count + 1;
            _referees.Add(ref1);
            Referee ref2 = new Referee("Kevin Scott", 24, 1.1, RefereeType.ALTERNATE);
            ref2.RefereeId = _referees.Count + 1;
            _referees.Add(ref2);
            Referee ref3 = new Referee("Ray Acosta", 54, 0.6, RefereeType.REFEREE);
            ref3.RefereeId = _referees.Count + 1;
            _referees.Add(ref3);
            Referee ref4 = new Referee("Tre Maddox", 23, -1, RefereeType.REFEREE);
            ref4.RefereeId = _referees.Count + 1;
            _referees.Add(ref4);
            Referee ref5 = new Referee("Scott Foster", 48, 7.5, RefereeType.CREW_CHIEF);
            ref5.RefereeId = _referees.Count + 1;
            _referees.Add(ref5);

            //voeg de matches toe aan de overeenstemmende arena
            arena1.Matches.Add(match5);
            arena2.Matches.Add(match2);
            arena2.Matches.Add(match4);
            arena3.Matches.Add(match1);
            arena3.Matches.Add(match3);
        }

        public void CreateMatch(Match match)
        {
            match.MatchId = _matches.Count + 1;
            _matches.Add(match);
        }

        public Match ReadMatch(int matchId)
        {
            foreach (Match m in _matches)
            {
                if (m.MatchId == matchId) return m;
            }
            return null;
        }

        public IEnumerable<Match> ReadAllMatches()
        {
            return _matches;
        }

        public IEnumerable<Match> ReadAllMatchesWithArena()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Match> ReadAllMatchesWithReferees()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Match> ReadMatchesOfFranchiseAndDate(string franchise, DateTime? datum)
        {
            List<Match> matches = new List<Match>();
            foreach (Match match in _matches)
                {
                    if (match.HomeTeam.ToLower().Contains(franchise.ToLower()) ||
                        match.AwayTeam.ToLower().Contains(franchise.ToLower()))
                    {    
                        if (datum.Equals(null))
                        {
                            matches.Add(match);
                        }
                        else if(match.Date.Equals(datum))
                        {
                            matches.Add(match);
                        }
                    }
                    else
                    {
                        if (datum.Equals(null))
                        {
                            matches.Add(match);
                        }
                        else if(match.Date.Equals(datum))
                        {
                            matches.Add(match);
                        }
                    }
                }
            return matches;
        }
        
        public IEnumerable<Match> ReadMatchesOfReferee(int refereeId)
        {
            throw new NotImplementedException();
        }

        
        
        
        public void CreateReferee(Referee referee)
        {
            referee.RefereeId = _referees.Count + 1;
            _referees.Add(referee);
        }

        public Referee ReadReferee(int refereeId)
        {
            foreach (Referee r in _referees)
            {
                if (r.RefereeId == refereeId) return r;
            }

            return null;
        }

        public IEnumerable<Referee> ReadAllReferees()
        {
            return _referees;
        }

        public IEnumerable<Referee> ReadRefereesOfType(int type)
        {
            List<Referee> referees = new List<Referee>();
            foreach (Referee r in _referees)
            {
                if ((int) r.Type == type) referees.Add(r);
            }

            return referees;
        }

        public IEnumerable<Referee> ReadRefereesAssignedToMatch(int matchId)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Referee> ReadRefereesNotAssignedToMatch(int matchId)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Referee> ReadAllRefereesOfMatch(int matchId)
        {
            throw new NotImplementedException();
        }

        
        
        public void CreateArena(Arena arena)
        {
            arena.ArenaId = _arenas.Count + 1;
            _arenas.Add(arena);
        }
        
        public Arena ReadArena(String name)
        {
            foreach (Arena a in _arenas)
            {
                if (a.Name.ToLower().Equals(name.ToLower()))
                {
                    return a;
                }
            }

            return null;
        }




        public MatchReferee ReadMatchReferee(int matchId, int refereeId)
        {
            throw new NotImplementedException();
        }

        public void AddRefereeToMatch(int matchId, int refereeId)
        {
            throw new NotImplementedException();
        }

        public void DeleteRefereeFromMatch(int matchId, int refereeId)
        {
            throw new NotImplementedException();
        }
    }
}