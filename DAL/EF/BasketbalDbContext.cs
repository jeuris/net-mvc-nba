﻿using Basketbal.BL.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Basketbal.DAL.EF
{
    internal class BasketbalDbContext : DbContext
    {
        public DbSet<Arena> Arenas { get; set; }
        public DbSet<Match> Matches { get; set; }
        public DbSet<Referee> Referees { get; set; }
        public DbSet<MatchReferee> MatchReferees { get; set; }

        public BasketbalDbContext()
        {
            BasketbalInitializer.Initialize(this, dropDb:false);
        }
        
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);

            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlite("Data Source=../BasketbalDatabase.db"); //in praktijk relatief pad
                optionsBuilder.UseLoggerFactory(LoggerFactory.Create(logBuilder => logBuilder.AddDebug()));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            // * - 1 relation
            modelBuilder.Entity<Match>()
                .HasOne(m => m.Arena).WithMany(a => a.Matches)
                .HasForeignKey("ArenaId");

            // * - *  relation
            modelBuilder.Entity<MatchReferee>().HasOne(mr => mr.Match)
                .WithMany(m => m.Referees).HasForeignKey("MatchId_FK").IsRequired(); //kon ook via shadow prop alleen moet je daar naming convention volgen
            modelBuilder.Entity<MatchReferee>().HasOne(mr => mr.Referee)
                .WithMany(m => m.Matches).HasForeignKey("RefereeId_FK").IsRequired();
            
            modelBuilder.Entity<MatchReferee>()
                .HasKey("MatchId_FK", "RefereeId_FK");

            modelBuilder.Entity<Match>().HasMany(mr => mr.Referees);
            modelBuilder.Entity<Referee>().HasMany(mr => mr.Matches);
        }
    }
}