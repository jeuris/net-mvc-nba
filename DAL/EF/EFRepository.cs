﻿using System;
using System.Collections.Generic;
using System.Linq;
using Basketbal.BL.Domain;
using Microsoft.EntityFrameworkCore;

namespace Basketbal.DAL.EF
{
    public class EFRepository : IRepository
    {
        private readonly BasketbalDbContext _dbContext;

        public EFRepository()
        {
            _dbContext = new BasketbalDbContext();
        }
        
        public void CreateMatch(Match match)
        {
            _dbContext.Matches.Add(match);
            _dbContext.SaveChanges();
        }

        public Match ReadMatch(int matchId)
        {
            return _dbContext.Matches.Find(matchId); //gaat eerst kijken in memory, dan pas in databank | single kijkt altijd in db
        }

        public IEnumerable<Match> ReadAllMatches()
        {
            return _dbContext.Matches.AsEnumerable();
        }

        public IEnumerable<Match> ReadAllMatchesWithArena()
        {
            return _dbContext.Matches.Include(m => m.Arena).AsEnumerable();
        }

        public IEnumerable<Match> ReadAllMatchesWithReferees()
        {
            return _dbContext.Matches
                .Include(m => m.Referees)
                .ThenInclude(mr => mr.Referee)
                .AsEnumerable();
        }

        public IEnumerable<Match> ReadMatchesOfFranchiseAndDate(string franchise, DateTime? datum) 
            //return IEnum omdat als de BL dan nog aanpassingen wil maken, deze in memory gebeuren
        {
            IQueryable<Match> matches = _dbContext.Matches;
            
            if (!string.IsNullOrWhiteSpace(franchise))
            {
                matches = matches.Where
                (m => (m.HomeTeam.ToLower().Contains(franchise.ToLower()) || m.AwayTeam.ToLower().Contains(franchise.ToLower())));
            }
            if (!datum.Equals(null))
            {
                matches = matches.Where(m => datum.Equals(m.Date));
            }
            return matches;
        }
        
        public IEnumerable<Match> ReadMatchesOfReferee(int refereeId)
        {
            ICollection<Match> matches = new List<Match>();
            IQueryable<MatchReferee> matchReferees = _dbContext.MatchReferees
                .Include(mr => mr.Match)
                .Where(mr => mr.Referee.RefereeId == refereeId);

            foreach (MatchReferee matchReferee in matchReferees)
            {
                matches.Add(matchReferee.Match);
            }
            
            return matches;
        }
        
        
        
        

        public void CreateReferee(Referee referee)
        {
            _dbContext.Referees.Add(referee);
            _dbContext.SaveChanges();
        }

        public Referee ReadReferee(int refereeId)
        {
            return _dbContext.Referees.Find(refereeId);
        }

        public IEnumerable<Referee> ReadAllReferees()
        {
            return _dbContext.Referees.AsEnumerable();
        }

        public IEnumerable<Referee> ReadRefereesOfType(int type)
        {
            return _dbContext.Referees.Where(r => (int)r.Type == type); //call van link op dbset is Iqueryable
        }

        public IEnumerable<Referee> ReadRefereesAssignedToMatch(int matchId)
        {
            ICollection<Referee> referees = new List<Referee>();
            IQueryable<MatchReferee> matchReferees = _dbContext.MatchReferees
                .Include(mr => mr.Referee)
                .Where(mr => mr.Match.MatchId == matchId);
                
            foreach (MatchReferee matchReferee in matchReferees)
            {
                referees.Add(matchReferee.Referee);
            }

            return referees;
        }

        public IEnumerable<Referee> ReadRefereesNotAssignedToMatch(int matchId)
        {
            ICollection<Referee> referees = new List<Referee>(); 
            foreach (Referee referee in _dbContext.Referees) //vul lijst met alle referee objecten
            {
                referees.Add(referee);
            }
            IQueryable<MatchReferee> matchReferees = _dbContext.MatchReferees
                .Include(mr => mr.Referee)
                .Where(mr => mr.Match.MatchId == matchId);

            foreach (MatchReferee matchReferee in matchReferees)
            {
                if (referees.Contains(matchReferee.Referee)) referees.Remove(matchReferee.Referee); 
                //remove de referees die al toegewezen waren aan de match met de matchid
            }
            
            return referees;
        }

        
        
        
        
        public void CreateArena(Arena arena)
        {
            _dbContext.Arenas.Add(arena);
            _dbContext.SaveChanges();
        }

        public Arena ReadArena(string name)
        {
            return _dbContext.Arenas.SingleOrDefault(a => a.Name.ToLower().Equals(name.ToLower()));
        }

        
        
        
        
        public MatchReferee ReadMatchReferee(int matchId, int refereeId)
        {
            return _dbContext.MatchReferees.Find(matchId, refereeId);
        }

        public void AddRefereeToMatch(int matchId, int refereeId)
        {
            MatchReferee matchReferee = new MatchReferee(_dbContext.Matches.Find(matchId),_dbContext.Referees.Find(refereeId));
            _dbContext.MatchReferees.Add(matchReferee);
            _dbContext.SaveChanges();
        }

        public void DeleteRefereeFromMatch(int matchId, int refereeId)
        {
            _dbContext.MatchReferees.Remove(_dbContext.MatchReferees.Find(matchId, refereeId));
            _dbContext.SaveChanges();
        }
    }
}