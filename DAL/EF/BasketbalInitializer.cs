﻿using System;
using System.Collections.Generic;
using System.Linq;
using Basketbal.BL.Domain;
using Microsoft.EntityFrameworkCore;

namespace Basketbal.DAL.EF
{
    internal static class BasketbalInitializer //internal zodat het enkel binnen deze assembly gebruikt kan worden
    {
        private static bool _isInitialized = false;

        public static void Initialize(BasketbalDbContext dbContext, bool dropDb)
        {
            if (!_isInitialized) //calls naar de EnsureCreated methode zo laag mogelijk houden
            {
                if (dropDb)
                {
                    dbContext.Database.EnsureDeleted();
                }

                if (dbContext.Database.EnsureCreated()) //nieuw schema aanmaken
                {
                    _isInitialized = true;
                    Seed(dbContext);
                }
            }
        }

        private static void Seed(BasketbalDbContext dbContext)
        {
            Arena arena1 = new Arena("FedExForum", "Memphis", 18199);
            Arena arena2 = new Arena("Pepsi Center", "Denver", 20000);
            Arena arena3 = new Arena("TD Garden", "Boston", 18624);

            Match match1 = new Match("Boston Celtics", "Philadelphia 76ers",
                new DateTime(2020, 2, 2), arena3);
            Match match2 = new Match("Denver Nuggets", "Houston Rockets",
                new DateTime(2020, 1, 26), arena2);
            Match match3 = new Match("Boston Celtics", "Miami Heat",
                new DateTime(2020, 9, 23), arena3);
            Match match4 = new Match("Denver Nuggets", "Dallas Mavericks",
                new DateTime(2019, 10, 29), arena2);
            Match match5 = new Match("Memphis Grizzlies", "Washington Wizards",
                new DateTime(2019, 12, 30), arena1);
            
            Referee ref1 = new Referee("Josh Tiven", 58, 0.6, RefereeType.ALTERNATE);
            Referee ref2 = new Referee("Kevin Scott", 24, 1.1, RefereeType.ALTERNATE);
            Referee ref3 = new Referee("Ray Acosta", 54, 0.6, RefereeType.REFEREE);
            Referee ref4 = new Referee("Tre Maddox", 23, -1, RefereeType.REFEREE);
            Referee ref5 = new Referee("Scott Foster", 48, 7.5, RefereeType.CREW_CHIEF);

            //wijs de bij-elkaar-horende referees & matches toe aan een MatchReferee object
            MatchReferee mr1 = new MatchReferee(match1, ref1);
            MatchReferee mr2 = new MatchReferee(match1, ref2);
            MatchReferee mr3 = new MatchReferee(match2, ref3);
            MatchReferee mr4 = new MatchReferee(match2, ref4);
            MatchReferee mr5 = new MatchReferee(match3, ref4);
            MatchReferee mr6 = new MatchReferee(match3, ref5);
            MatchReferee mr7 = new MatchReferee(match4, ref1);
            MatchReferee mr8 = new MatchReferee(match4, ref4);
            MatchReferee mr9 = new MatchReferee(match5, ref2);
            MatchReferee mr10 = new MatchReferee(match5, ref3);
            
            //voeg de MatchReferee objecten toe aan de MR lijst van de klasse Match
            match1.Referees = new List<MatchReferee> {mr1, mr2};
            match2.Referees = new List<MatchReferee> {mr3, mr4};
            match3.Referees = new List<MatchReferee> {mr5, mr6};
            match4.Referees = new List<MatchReferee> {mr7, mr8};
            match5.Referees = new List<MatchReferee> {mr9, mr10};

            //voeg de matches toe aan de overeenstemmende arena
            arena1.Matches= new List<Match> { match5 };
            arena2.Matches= new List<Match> { match2, match4 };
            arena3.Matches= new List<Match> { match1, match3 };

            
            dbContext.Matches.Add(match1);
            dbContext.Matches.Add(match2);
            dbContext.Matches.Add(match3);
            dbContext.Matches.Add(match4);
            dbContext.Matches.Add(match5);
            dbContext.Referees.Add(ref1);
            dbContext.Referees.Add(ref2);
            dbContext.Referees.Add(ref3);
            dbContext.Referees.Add(ref4);
            dbContext.Referees.Add(ref5);
            dbContext.MatchReferees.Add(mr1);
            dbContext.MatchReferees.Add(mr2);
            dbContext.MatchReferees.Add(mr3);
            dbContext.MatchReferees.Add(mr4);
            dbContext.MatchReferees.Add(mr5);
            dbContext.MatchReferees.Add(mr6);
            dbContext.MatchReferees.Add(mr7);
            dbContext.MatchReferees.Add(mr8);
            dbContext.MatchReferees.Add(mr9);
            dbContext.MatchReferees.Add(mr10);
            
            dbContext.SaveChanges();

            foreach (var entry in dbContext.ChangeTracker.Entries().ToList())
            {
                entry.State = EntityState.Detached;
            }
        }
    }
}