﻿using System;
using System.Collections.Generic;
using Basketbal.BL.Domain;

namespace Basketbal.DAL
{
    public interface IRepository
    {
        abstract void CreateMatch(Match match);
        abstract Match ReadMatch(int matchId);
        abstract IEnumerable<Match> ReadAllMatches();
        abstract IEnumerable<Match> ReadAllMatchesWithArena();
        abstract IEnumerable<Match> ReadAllMatchesWithReferees();
        abstract IEnumerable<Match> ReadMatchesOfFranchiseAndDate(string franchise, DateTime? datum);
        abstract IEnumerable<Match> ReadMatchesOfReferee(int refereeId);
        
        abstract void CreateReferee(Referee referee);
        abstract Referee ReadReferee(int refereeId);
        abstract IEnumerable<Referee> ReadAllReferees();
        abstract IEnumerable<Referee> ReadRefereesOfType(int type);
        abstract IEnumerable<Referee> ReadRefereesAssignedToMatch(int matchId);
        abstract IEnumerable<Referee> ReadRefereesNotAssignedToMatch(int matchId);

        abstract void CreateArena(Arena arena);
        abstract Arena ReadArena(string name);

        abstract MatchReferee ReadMatchReferee(int matchId, int refereeId);
        abstract void AddRefereeToMatch(int matchId, int refereeId);
        abstract void DeleteRefereeFromMatch(int matchId, int refereeId);
    }
}