﻿using System.Collections.Generic;
using Basketbal.BL;
using Basketbal.BL.Domain;
using Microsoft.AspNetCore.Mvc;

namespace Basketbal.UI.MVC.Controllers
{
    public class RefereeController : Controller
    {
        private IManager _mgr;

        public RefereeController(IManager mgr)
        {
            _mgr = mgr;
        }
        
        public ActionResult Index()
        {
            return View();
        }
        
        public IActionResult Detail(int refereeId)
        {
            Referee referee = _mgr.GetReferee(refereeId);
            return View(referee);
        }

        public IActionResult SelectRefereeToAddToMatch(int MatchId)
        {
            IEnumerable<Referee> referees = _mgr.GetAllRefereesNotAssignedToMatch(MatchId);
            ViewBag.MatchId = MatchId;
            return View(referees);
        }
        
        public IActionResult SelectRefereeToRemoveFromMatch(int MatchId)
        {
            IEnumerable<Referee> referees = _mgr.GetAllRefereesAssignedToMatch(MatchId);
            ViewBag.MatchId = MatchId;
            return View(referees);
        }
    }
}