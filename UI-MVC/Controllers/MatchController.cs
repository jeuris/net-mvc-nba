﻿using System;
using System.Collections.Generic;
using Basketbal.BL;
using Basketbal.BL.Domain;
using Microsoft.AspNetCore.Mvc;

namespace Basketbal.UI.MVC.Controllers
{
    public class MatchController : Controller
    {
        private IManager _mgr;

        public MatchController(IManager mgr)
        {
            _mgr = mgr;
        }
        
        public IActionResult Index()
        {
            IEnumerable<Match> matches = _mgr.GetAllMatches();
            return View(matches);
        }

        public IActionResult Detail(int matchId)
        {
            Match match = _mgr.GetMatch(matchId);
            IEnumerable<Referee> referees = _mgr.GetAllRefereesAssignedToMatch(matchId);
            // kan ook 1 methode van _mgr oproepen die met include alles in 1x binnenhaalt, maar wou met viewbag experimenteren
            ViewBag.Referees = referees;
            return View(match);
        }

        [HttpGet]
        public IActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Add(Match match)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            _mgr.AddMatch(match.HomeTeam,match.AwayTeam, match.Date.ToString(), null);
            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult MatchFilterFranchiseAndDate()
        {
            return View();
        }

        public IActionResult MatchesOfFranchiseAndDate(string franchiseInput, string dateInput)
        {
            IEnumerable<Match> matches = _mgr.GetAllMatchesOfFranchiseAndDate(franchiseInput, dateInput);
            return View(matches);
        }
        
        public IActionResult SelectMatchToAddRefereeTo()
        {
            IEnumerable<Match> matches = _mgr.GetAllMatches();
            return View(matches);
        }

        public IActionResult AddRefereeToMatch(int MatchId, int RefereeId)
        {
            _mgr.AddRefereeToMatch(MatchId, RefereeId);
            return RedirectToAction("Detail", "Match", new {matchId = MatchId});
        }
        
        public IActionResult SelectMatchToRemoveRefereeFrom()
        {
            IEnumerable<Match> matches = _mgr.GetAllMatches();
            return View(matches);
        }

        public IActionResult RemoveRefereeFromMatch(int MatchId, int RefereeId)
        {
            _mgr.RemoveRefereeFromMatch(MatchId, RefereeId);
            return RedirectToAction("Detail", "Match", new {matchId = MatchId});
        }
    }
}