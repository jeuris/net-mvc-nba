﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Basketbal.BL.Domain
{
    public class Match : IValidatableObject
    {
        public int MatchId { get; set; }
        
        [Required] [StringLength(40,MinimumLength = 3)] public string HomeTeam { get; set; }
        
        [Required] [StringLength(40,MinimumLength = 3)] public string AwayTeam { get; set; }
        
        [Required] public DateTime Date { get; set; }

        public Arena Arena { get; set; }
        
        public ICollection<MatchReferee> Referees { get; set; }
        
        public Match(){}
        public Match(string home, string away, DateTime date, Arena arena)
        {
            HomeTeam = home;
            AwayTeam = away;
            Date = date;
            Arena = arena;
            Referees = new List<MatchReferee>();
        }
        
        
        
        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var errors = new List<ValidationResult>();
            if (this.Date > DateTime.Now)
            {
                String error = "Datum ligt in de toekomst";
                errors.Add(new ValidationResult(error, new String[]{"Date"}));
            }
            return errors;
        }
    }
}