﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Basketbal.BL.Domain
{
    public class Arena
    {
        public int ArenaId { get; set; }
        
        [Required] public string Name{ set; get; }
        
        [Required] public string Country { get; set; }
        
        public int Capacity { get; set; }
        
        public List<Match> Matches { get; set; }

        public Arena() { }
        public Arena(string name, string country, int capacity)
        {
            Name = name;
            Country = country;
            Capacity = capacity;
            Matches = new List<Match>();
        }
    }
}