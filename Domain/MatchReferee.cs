﻿using System.ComponentModel.DataAnnotations;

namespace Basketbal.BL.Domain
{
    public class MatchReferee
    {
        [Required] public Match Match { get; set; }
        [Required] public Referee Referee { get; set; }
        
        public MatchReferee(){}

        public MatchReferee(Match match, Referee referee)
        {
            Match = match;
            Referee = referee;
        }
    }
}