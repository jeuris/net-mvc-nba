﻿namespace Basketbal.BL.Domain
{
    public enum RefereeType
    {
        CREW_CHIEF=1, REFEREE=2, UMPIRE=3, ALTERNATE=4
        //database werkt achterliggend met de numerieke waardes
    }
}