﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Basketbal.BL.Domain
{
    public class Referee
    {
        public int RefereeId { get; set; }
        
        [Required] public string Name { get; set; }
        
        public int? UniformNumber { get; set; }
        
        [Range(-10.00,10.00)] public double FoulDifferential { get; set; }
        
        public RefereeType Type { get; set; }

        public ICollection<MatchReferee> Matches { get; set; } 
        
        public Referee(){}
        public Referee(string name, int? number, double foulDiff, RefereeType type)
        {
            Name = name;
            UniformNumber = number;
            FoulDifferential = foulDiff;
            Type = type;
            Matches = new List<MatchReferee>();
        }
    }
}