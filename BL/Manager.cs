﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Basketbal.BL.Domain;
using Basketbal.DAL;
using Basketbal.DAL.EF;

namespace Basketbal.BL
{
    public class Manager : IManager
    {
        private readonly IRepository _efRepository;

        public Manager()
        {
            _efRepository = new EFRepository();
        }

        public void AddMatch(string home, string away, string date, Arena arena)
        //datum komt hier nog als string binnen om eerst de user-input te valideren
        {
            if (!ValidateDate(date))
            {
                throw new FormatException("Input must be a date (eg: dd/MM/yyyy)");
            }
            Match match = new Match(home,away,DateTime.Parse(date),arena);
            this.Validate(match);
            _efRepository.CreateMatch(match);
        }

        public Match GetMatch(int matchId)
        {
            return _efRepository.ReadMatch(matchId);
        }

        public IEnumerable<Match> GetAllMatches()
        {
            return _efRepository.ReadAllMatches();
        }

        public IEnumerable<Match> GetAllMatchesWithArena()
        {
            return _efRepository.ReadAllMatchesWithArena();
        }

        public IEnumerable<Match> GetAllMatchesWithReferees()
        {
            return _efRepository.ReadAllMatchesWithReferees();
        }

        public IEnumerable<Match> GetAllMatchesOfFranchiseAndDate(string franchise, string datum) 
        //datum komt hier nog als string binnen om eerst de user-input te valideren
        {
            if (string.IsNullOrEmpty(datum))
            {
                return _efRepository.ReadMatchesOfFranchiseAndDate(franchise, null);
            }
            if (ValidateDate(datum))
            {
                return _efRepository.ReadMatchesOfFranchiseAndDate(franchise, DateTime.Parse(datum));
            }
            throw new FormatException("Input must be a date (eg: dd/MM/yyyy)");
        }

        public IEnumerable<Match> GetAllMatchesOfReferee(int refereeId)
        {
            return _efRepository.ReadMatchesOfReferee(refereeId);
        }
        
        
        
        
        public void AddReferee(string name, int? number, double foulDiff, RefereeType type)
        {
            Referee referee = new Referee(name,number,foulDiff, type);
            this.Validate(referee);
            _efRepository.CreateReferee(referee);
        }

        public Referee GetReferee(int refereeId)
        {
            return _efRepository.ReadReferee(refereeId);
        }

        public IEnumerable<Referee> GetAllReferees()
        {
            return _efRepository.ReadAllReferees();
        }

        public IEnumerable<Referee> GetAllRefereesOfType(int type)
        {
            return _efRepository.ReadRefereesOfType(type);
        }

        public IEnumerable<Referee> GetAllRefereesAssignedToMatch(int matchId)
        {
            return _efRepository.ReadRefereesAssignedToMatch(matchId);
        }

        public IEnumerable<Referee> GetAllRefereesNotAssignedToMatch(int matchId)
        {
            return _efRepository.ReadRefereesNotAssignedToMatch(matchId);
        }

        
        
        
        public void AddArena(string name, string country, int capacity)
        {
            Arena arena = new Arena(name, country, capacity);
            this.Validate(arena);
            _efRepository.CreateArena(arena);
        }

        public Arena GetArena(string name)
        {
            return _efRepository.ReadArena(name);
        }

        
        
        
        public void AddRefereeToMatch(int matchId, int refereeId)
        {
            if (_efRepository.ReadMatchReferee(matchId,refereeId) == null)
            {
                if (_efRepository.ReadMatch(matchId) == null) throw new Exception("This match doesn't exist");

                if (_efRepository.ReadReferee(refereeId) == null) throw new Exception("This referee doesn't exist");
                
                _efRepository.AddRefereeToMatch(matchId, refereeId);
            }
            else
            {
                throw new Exception("This Referee is already a part of this match.");   
            }
        }
        
        public void RemoveRefereeFromMatch(int matchId, int refereeId)
        {
            if (_efRepository.ReadMatchReferee(matchId,refereeId) != null)
            {
                _efRepository.DeleteRefereeFromMatch(matchId, refereeId);
            }
            else
            {
                throw new Exception("This Referee is not a part of this match.");   
            }
        }
        
        private void Validate(Object o) //validate dataAnnotations van object
        {
            List<ValidationResult> errors = new List<ValidationResult>();
            bool valid = Validator.TryValidateObject(o, new ValidationContext(o), errors,true);
            StringBuilder allErrors = new StringBuilder();
            if (!valid) {
                foreach (var er in errors)
                {
                    allErrors.Append(er.ErrorMessage + "\n");
                }
                throw new ValidationException(allErrors.ToString());
            }
        }

        private Boolean ValidateDate(string datum)
        {
            DateTime checkdate;
            return DateTime.TryParse(datum, out checkdate);
        }
    }
}