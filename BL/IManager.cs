﻿using System;
using System.Collections.Generic;
using Basketbal.BL.Domain;

namespace Basketbal.BL
{
    public interface IManager
    {
        abstract void AddMatch(string home, string away, string date, Arena arena);
        abstract Match GetMatch(int matchId);
        abstract IEnumerable<Match> GetAllMatches();
        abstract IEnumerable<Match> GetAllMatchesWithArena();
        abstract IEnumerable<Match> GetAllMatchesWithReferees();
        abstract IEnumerable<Match> GetAllMatchesOfFranchiseAndDate(string franchise, string datum);
        abstract IEnumerable<Match> GetAllMatchesOfReferee(int refereeId);

        abstract void AddReferee(string name, int? number, double foulDiff, RefereeType type);
        abstract Referee GetReferee(int refereeId);
        abstract IEnumerable<Referee> GetAllReferees();
        abstract IEnumerable<Referee> GetAllRefereesOfType(int type);
        abstract IEnumerable<Referee> GetAllRefereesAssignedToMatch(int matchId);
        abstract IEnumerable<Referee> GetAllRefereesNotAssignedToMatch(int matchId);

        abstract void AddArena(string name, string country, int capacity);
        abstract Arena GetArena(string name);
        
        abstract void AddRefereeToMatch(int matchId, int refereeId);
        abstract void RemoveRefereeFromMatch(int matchId, int refereeId);
    }
}