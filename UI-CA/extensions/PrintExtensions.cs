﻿using System;
using System.Text;
using Basketbal.BL.Domain;

namespace Basketbal.UI.CA.extensions
{
    internal static class PrintExtensions
    {
        public static string GetRefereeSummary(this Referee referee)
        {
            return String.Format("{0} {1} (Uniform No.{2}) has a foul differential of {3}",
                referee.Type, referee.Name, referee.UniformNumber, referee.FoulDifferential);
        }
        
        public static string GetMatchSummary(this Match match)
        {
            return String.Format("Match {0}, played on {1}, features {2} and {3}",
                match.MatchId, match.Date.ToString("dd/MM/yyyy"), match.HomeTeam, match.AwayTeam);
        }
        
        public static string GetArenaSummary(this Arena arena)
        {
            return String.Format("{0} located in {1} has a capacity of {2}",
                arena.Name, arena.Country, arena.Capacity);
        }
        
        public static string GetMatchWithArenaSummary(this Match match)
        {
            return String.Format("{0} \n\t {1}",
                match.GetMatchSummary(), match.Arena.GetArenaSummary());
        }
        
        public static string GetMatchWithRefereesSummary(this Match match)
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append(match.GetMatchSummary() + "\n\t");
            foreach (MatchReferee matchReferee in match.Referees)
            {
                stringBuilder.Append(matchReferee.Referee.GetRefereeSummary() + "\n\t");
            }
            return stringBuilder.ToString();
        }
    }
}