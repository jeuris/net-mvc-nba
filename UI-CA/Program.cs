﻿using System;
using System.ComponentModel.DataAnnotations;
using Basketbal.BL;
using Basketbal.BL.Domain;
using Basketbal.UI.CA.extensions;

namespace Basketbal.UI.CA
{
    class Program
    {
        private static readonly IManager _manager = new Manager();
        private static bool _again = true;

        static void Main(string[] args)
        {
            while (_again)
            {
                try
                {
                    Intro();
                    int answer = Int32.Parse(Console.ReadLine());
                    Console.WriteLine();
                    switch (answer)
                    {
                        case 0:
                            _again = false;
                            return;
                        case 1:
                            ShowAllReferees();
                            break;
                        case 2:
                            ShowRefereesOfType();
                            break;
                        case 3:
                            ShowAllMatches();
                            break;
                        case 4:
                            ShowAllMatchesWithReferees();
                            break;
                        case 5:
                            ShowAllMatchesWithArena();
                            break;
                        case 6:
                            ShowMatchesOfFranchiseAndDate();
                            break;
                        case 7:
                            AddMatchMethod();
                            break;
                        case 8:
                            AddRefereeMethod();
                            break;
                        case 9:
                            AddRefereeToMatchMethod();
                            break;
                        case 10:
                            RemoveRefereeFromMatchMethod();
                            break;
                        default:
                            Console.WriteLine("Please enter a correct number (0-10)");
                            break;
                    }
                }
                catch (ValidationException ex)
                {
                    Console.WriteLine(ex.Message);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message + " Try again!");
                }
            }
        }

        private static void Intro()
        {
            Console.WriteLine("\nWhat would you like to do?");
            Console.WriteLine("==========================");
            Console.WriteLine("0) Quit");
            Console.WriteLine("1) Show all Referees");
            Console.WriteLine("2) Show Referees of specific type");
            Console.WriteLine("3) Show all Matches");
            Console.WriteLine("4) Show all Matches with referees");
            Console.WriteLine("5) Show all Matches with arenas");
            Console.WriteLine("6) Show matches of specific date/team");
            Console.WriteLine("7) Add a match");
            Console.WriteLine("8) Add a referee");
            Console.WriteLine("9) Add Referee to Match");
            Console.WriteLine("10) Remove Referee from match");
            Console.WriteLine("==========================");
            Console.Write("Choice (0-10): ");
        }

        private static void ShowAllReferees()
        {
            foreach (Referee referee in _manager.GetAllReferees())
            {
                Console.WriteLine("["+referee.RefereeId+"] "+referee.GetRefereeSummary());
            }
        }

        private static void ShowRefereesOfType()
        {
            Console.Write("Type (1=Crew Chief, 2=referee, 3=umpire, 4=alternate): ");
            int type = Int32.Parse(Console.ReadLine());
            foreach (Referee referee in _manager.GetAllRefereesOfType(type))
            {
                Console.WriteLine("["+referee.RefereeId+"] "+referee.GetRefereeSummary());
            }
        }

        private static void ShowAllMatches()
        {
            foreach (Match match in _manager.GetAllMatches())
            {
                Console.WriteLine("["+match.MatchId+"] "+match.GetMatchSummary());
            }
        }
        
        private static void ShowAllMatchesWithReferees()
        {
            foreach (Match match in _manager.GetAllMatchesWithReferees())
            {
                Console.WriteLine("["+match.MatchId+"] "+match.GetMatchWithRefereesSummary());
            }
        }
        
        private static void ShowAllMatchesWithArena()
        {
            foreach (Match match in _manager.GetAllMatchesWithArena())
            {
                Console.WriteLine("["+match.MatchId+"] "+match.GetMatchWithArenaSummary());
            }
        }
        
        private static void ShowMatchesOfFranchiseAndDate()
        {
            Console.Write("Enter (part of) a franchise or leave blank: ");
            string franchise = Console.ReadLine();
            Console.Write("Enter a full date (eg: dd/MM/yyyy) or leave blank: ");
            string inputDate = Console.ReadLine();
            foreach (Match match in _manager.GetAllMatchesOfFranchiseAndDate(franchise, inputDate))
            {
                Console.WriteLine("["+match.MatchId+"] "+match.GetMatchSummary());
            }
        }

        private static void AddMatchMethod()
        {
            Console.Write("Home team: ");
            string home = Console.ReadLine();
            Console.Write("Away team: ");
            string away = Console.ReadLine();
            Console.Write("Date (eg: dd/MM/yyyy): ");
            string inputDate = Console.ReadLine();
            Console.Write("Arena (name only): ");
            String arenaName = Console.ReadLine();
            if (_manager.GetArena(arenaName) == null)
            {
                Console.WriteLine("You have to make an Arena first, only then can the match be added");
                Console.Write("Country: ");
                string country = Console.ReadLine();
                Console.Write("Capacity: ");
                int capacity = Int32.Parse(Console.ReadLine());
                _manager.AddArena(arenaName, country, capacity);
            }

            Arena arena = _manager.GetArena(arenaName);
            _manager.AddMatch(home, away, inputDate, arena);
        }

        private static void AddRefereeMethod()
        {
            Console.Write("Name: ");
            string name = Console.ReadLine();
            Console.Write("Uniform number (optional): ");
            string uniformNumber = Console.ReadLine();
            Console.Write("Fouldifferential: ");
            double foulDiff = Double.Parse(Console.ReadLine());
            Console.Write("Type (1=Crew Chief, 2=referee, 3=umpire, 4=alternate): ");
            int type = Int32.Parse(Console.ReadLine());
            
            //uniformnumber zou nullable int moeten zijn maar wanneer je empty input wil parsen naar INT32
            //wordt er een exception gegooid ipv null opgeslagen, dus sla ik het op in een string en test op ""
            
            int convertedNumber;
            if (uniformNumber.Equals("")) {
                _manager.AddReferee(name, null, foulDiff, (RefereeType) type);
            }
            else if (int.TryParse(uniformNumber, out convertedNumber)) {
                _manager.AddReferee(name, convertedNumber, foulDiff, (RefereeType) type);
            }else throw new ArgumentException("Uniform has to be a number");
        }

        private static void AddRefereeToMatchMethod()
        {
            Console.WriteLine("Which Match would you like to add a Referee to?");
            ShowAllMatches();
            Console.Write("Please enter the Match ID: ");
            int matchId = Int32.Parse(Console.ReadLine());

            Console.WriteLine("\nWhich Referee would you like to assign to this Match?");
            foreach (Referee referee in _manager.GetAllRefereesNotAssignedToMatch(matchId))
            {
                Console.WriteLine("["+referee.RefereeId+"] "+referee.GetRefereeSummary());
            }
            Console.Write("Please enter the Referee ID: ");
            int refereeId = Int32.Parse(Console.ReadLine());
            
            _manager.AddRefereeToMatch(matchId,refereeId);
        }

        private static void RemoveRefereeFromMatchMethod()
        {
            Console.WriteLine("Which match would you like to remove a Referee from?");
            ShowAllMatches();
            Console.Write("Please enter the Match ID: ");
            int matchId = Int32.Parse(Console.ReadLine());

            Console.WriteLine("\nWhich referee would you like to remove from this Match?");
            foreach (Referee referee in _manager.GetAllRefereesAssignedToMatch(matchId))
            {
                Console.WriteLine("["+referee.RefereeId+"]"+referee.GetRefereeSummary());
            }
            Console.Write("Please enter the Referee ID: ");
            int refereeId = Int32.Parse(Console.ReadLine());
            
            _manager.RemoveRefereeFromMatch(matchId,refereeId);
        }
    }
}